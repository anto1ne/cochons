# Rentabilité Cochons

Cette application réalise des calculs pour juger approximativement de la rentabilité d'une porcherie.

## Démonstration

![demo](demo.png)

## Remerciements

Merci à Wikimédia pour fournir l'[image du cochon](https://commons.wikimedia.org/wiki/File:Picture_pig.jpg) sous licence CC0.