#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 31 15:32:16 2020

@author: antoine
"""

import sys
from PyQt6 import QtWidgets, uic
from PyQt6.QtCore import QUrl
from PyQt6.QtGui import QDesktopServices

from fonctions import Rentabilite, Poids, read_infos, save_infos
       
class Ui(QtWidgets.QMainWindow):
    def __init__(self):
        super(Ui, self).__init__()
        uic.loadUi('gui.ui', self)
        self.init_value()
        self.calcul_renta()
        self.calcul_poids()
        
        # tab renta
        self.nb_cochons.valueChanged.connect(self.calcul_renta)
        self.perte_cochons.valueChanged.connect(self.calcul_renta)
        self.poids_moyen.valueChanged.connect(self.calcul_renta)
        self.prix_au_kg.valueChanged.connect(self.calcul_renta)
        self.prix_cochons.valueChanged.connect(self.calcul_renta)
        self.alimentations.valueChanged.connect(self.calcul_renta)
        
        self.lien_cours.clicked.connect(self.lancer_url)
        
        # tab poids
        self.tour_de_poitrine.valueChanged.connect(self.calcul_poids)
    
    def init_value(self):
        data = read_infos()
        
        self.nb_cochons.setValue(data["nb_cochons"])
        self.perte_cochons.setValue(data["perte_cochons"])
        self.poids_moyen.setValue(data["poids_moyen"])
        self.prix_au_kg.setValue(data["prix_au_kg"])
        self.prix_cochons.setValue(data["prix_cochons"])
        self.alimentations.setValue(data["alimentations"])
    
    def lancer_url(self):
        QDesktopServices.openUrl(QUrl('https://www.marche-porc-francais.com/'))
    
    def calcul_renta(self):
        nb_cochons = self.nb_cochons.value()
        perte_cochons = self.perte_cochons.value()
        poids_moyen = self.poids_moyen.value()
        prix_au_kg = self.prix_au_kg.value()
        prix_cochons = self.prix_cochons.value()
        alimentations = self.alimentations.value()
        
        renta = Rentabilite(nb_cochons, perte_cochons, poids_moyen, prix_au_kg, prix_cochons, alimentations)
        print(renta)
        
        self.label_revenus.setText("{:,.2f} €".format(renta.get_revenus()).replace(","," ").replace(".",","))
        self.label_depenses.setText("{:,.2f} €".format(renta.get_depenses()).replace(","," ").replace(".",","))
        
        self.label_resultat.setText(str(renta))
        
    def calcul_poids(self):
        tour_de_poitrine = self.tour_de_poitrine.value()
        poids = Poids(tour_de_poitrine)
        
        self.label_valeur_poids_vif.setText("{:.2f} kg".format(poids.get_poids_vif()).replace(".",","))
        self.label_valeur_poids_carcasse.setText("{:.2f} kg".format(poids.get_poids_carcasse()).replace(".",","))

    def closeEvent(self, *args, **kwargs):
        print("Sauvegarde en cours")
        
        data={}
        data["nb_cochons"]=self.nb_cochons.value()
        data["perte_cochons"]=self.perte_cochons.value()
        data["poids_moyen"]=self.poids_moyen.value()
        data["prix_au_kg"]=self.prix_au_kg.value()
        data["prix_cochons"]=self.prix_cochons.value()
        data["alimentations"]=self.alimentations.value()
        
        save_infos(data)
        
        super(Ui, self).closeEvent(*args, **kwargs)
        
if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = Ui()
    window.show()
    app.exec()
