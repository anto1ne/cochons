#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 31 17:24:16 2020

@author: antoine
"""

import json
from os.path import exists

# =============================================================================
FILE = "cochons.json" 

def read_infos():
    file_exists = exists(FILE)
    if file_exists:
        # Read data from file:
        data = json.load( open(FILE) )
    else :
        data={}
        data["nb_cochons"]=400
        data["perte_cochons"]=20
        data["poids_moyen"]=95
        data["prix_au_kg"]=1.5
        data["prix_cochons"]=50
        data["alimentations"]=60
    return data

def save_infos(data):
    # Serialize data into file:
    json.dump( data, open(FILE, 'w' ), indent = 4 )

# =============================================================================

class Poids():
    
    def __init__(self, tour_de_poitrine) :
        self.tour_de_poitrine = tour_de_poitrine
    
    def get_poids_vif(self):
        # https://www.publications.gov.on.ca/store/20170501121/Free_Download_Files/300225.pdf
        # Poids (lb) = 10,17 x tour de poitrine (pouces) – 205,75
        # return (10.17*self.tour_de_poitrine/2.54-205.75)/2.2
        
        # autre méthode
        # https://fluxdeconnaissances.com/information/page/read/173016-comment-calculer-le-poids-dun-cochon
        # Poids = 2,237 x TDP -124,985
        return (2.237*self.tour_de_poitrine-124.985)
    
    def get_poids_carcasse(self):
        return 0.8*self.get_poids_vif()

class Rentabilite():
    
    def __init__(self, nb_cochons, perte_cochons, poids_moyen, prix_au_kg, prix_cochons, alimentations):
        self.nb_cochons = nb_cochons
        self.perte_cochons = perte_cochons 
        self.poids_moyen = poids_moyen 
        self.prix_au_kg = prix_au_kg 
        self.prix_cochons = prix_cochons 
        self.alimentations = alimentations 
        
    def get_revenus(self):
        return (self.nb_cochons-self.perte_cochons)*self.poids_moyen*self.prix_au_kg
    
    def get_depenses(self):
        return (self.prix_cochons+self.alimentations)*self.nb_cochons
    
    def get_benefices(self):
        return self.get_revenus()-self.get_depenses()
    
    def get_prix_de_vente(self):
        return self.poids_moyen*self.prix_au_kg
    
    def get_prix_de_revient(self):
        return self.get_prix_de_vente()-self.get_benefices_par_cochons()
    
    def  get_benefices_par_cochons(self):
        return self.get_benefices()/self.nb_cochons
    
    def __str__(self):
        texte =("Bénéfices : {:,.2f} € \n"
        "Prix de vente par cochon : {:.2f} €/cochon \n"
        "Prix de revient par cochon : {:.2f} €/cochon \n"
        "Bénéfices par cochon : {:.2f} €/cochon \n").format(self.get_benefices(), self.get_prix_de_vente(), self.get_prix_de_revient(), self.get_benefices_par_cochons())
        return texte.replace(","," ").replace(".",",")
